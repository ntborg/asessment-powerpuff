import React, { Component } from 'react';
import ListItem from '../../components/ListItem/ListItem';
import './OverviewPage.scss';

class overviewPage extends Component {
    state = {
        error: null,
        isLoaded: true,
        items: []
    }

    componentDidMount() {
        fetch( 'https://api.tvmaze.com/shows/' + this.props.data.id + '/episodes')
            .then(res => res.json())
            .then(
                (res) => {
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;

        } else if (!isLoaded) {
            return <div>Loading...</div>;

        } else {
            // todo: add lazy loading
            // todo: bundle episode seasons
            // todo: onclick on the whole ListItem instead of Link
            const EpisodeItem = items.map( function(item, index) {
                return (
                    <ListItem key={index} data={item} />
                )
            }).reverse();

            return (
                <div className="OverviewPage">
                    <h1>{this.props.data.name}</h1>
                    <div className="OverviewPage-Summary">
                        <img src={this.props.data.image.medium} alt={`Image for: ${this.props.data.name}`} />
                        <div dangerouslySetInnerHTML={{__html: this.props.data.summary}}></div>
                    </div>
                    <div className="OverviewPage-Episodes">
                        <h2>Episodes</h2>
                        <ul>
                            {EpisodeItem}
                        </ul>
                    </div>
                </div>
            );
        }
    }
}

export default overviewPage;