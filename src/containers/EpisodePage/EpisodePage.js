import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './EpisodePage.scss';

class episodePage extends Component {

    state = {
        error: null,
        isLoaded: true,
        items: []
    }

    componentDidMount() {
        let seasonId = this.props.match.params.season;
        let episodeId = this.props.match.params.number;

        fetch( 'http://api.tvmaze.com/shows/6771/episodebynumber?season=' + seasonId + '&number=' + episodeId + '')
            .then(res => res.json())
            .then(
                (res) => {
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded} = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;

        } else if (!isLoaded) {
            return <div>Loading...</div>;

        } else {
            return (
                <div className="OverviewPage">

                    <h1>Season {this.state.items.season}, Episode {this.state.items.number}</h1>
                    <h2>{this.state.items.name}</h2>
                    <div className="OverviewPage-Summary">
                        {this.state.items.image !== null && this.state.items.image !== undefined &&
                        <img src={this.state.items.image.medium} alt={`Image for: ${this.state.items.name}`}/>
                        }
                        {this.state.items.image === null &&
                        <img src={process.env.PUBLIC_URL + '/Images/image-not-found.jpg'}
                             alt={`Image not found for: ${this.state.items.name}`}/>
                        }
                        <div dangerouslySetInnerHTML={{__html: this.state.items.summary}}></div>
                    </div>
                    <Link to="/" className="back">Back to overview</Link>
                </div>
            );
        }
    }
}

export default episodePage;