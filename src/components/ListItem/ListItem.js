import React from 'react';
import {Link} from "react-router-dom";

const listItem = (props) => {
    return (
        <li className="ListItem">
            {props.data.image !== null &&
                <img src={props.data.image.medium} alt={`Image for: ${props.data.name}`} />
            }
            {props.data.image === null &&
                <img src={process.env.PUBLIC_URL + '/Images/image-not-found.jpg'} alt={`Image not found for: ${props.data.name}`} />
            }
            <div>
                <h3>{props.data.name}</h3>
                <p><strong>Season: {props.data.season}</strong></p>
                <p>Airdate: {props.data.airdate}</p>
                <Link to={`/episode/${props.data.season}/${props.data.number}/`}>Details ></Link>
            </div>
        </li>
    )
};

export default listItem;