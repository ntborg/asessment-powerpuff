// todo: Add default loading component with axios(?)
// todo: Add fallback if there is no content to render
// todo: Centralize data with Redux
// todo: Centralize scss variables
// todo: Render styling component specific
// todo: Restyle the whole thing
// todo: Write automated tests
// todo: Refactor the whole thing. This can be a lot more efficient. The time tho...

import React, { Component } from 'react';
import {Route} from "react-router-dom";
import OverviewPage from "./containers/OverviewPage/OverviewPage";
import EpisodesPage from "./containers/EpisodePage/EpisodePage";
import './App.scss';

const routes = [
    {
        path: '/',
        component: OverviewPage,
        exact: true
    },
    {
        path: '/episode/:season/:number',
        component: EpisodesPage,
        exact: false
    }
];

class App extends Component {
    state = {
        error: null,
        isLoaded: false,
        // API_URL: 'https://api.tvmaze.com/shows/',
        // shows: [
        //     {
        //         id: 6771
        //     },
        //     {
        //         id: 1955
        //     },
        //     {
        //         id: 6769
        //     }
        // ],
        items: []
    };

    componentDidMount() {
        fetch('https://api.tvmaze.com/shows/6771')
            .then(res => res.json())
            .then(
                (res) => {
                    this.setState({
                        isLoaded: true,
                        items: res
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="App">
                    <main className="App-Content">
                        {routes.map(({path, component: C, exact}, index) => (
                            <Route
                                path={path}
                                render={(props) => <C {...props} data={items}/>}
                                key={index}
                                exact={exact}
                            />
                        ))}
                    </main>
                </div>
            );
        }
    }
}

export default App;
